@extends('albums.layout')

@section('content')
	
	<h2>Create a new album</h2>

	<div class="feedback"></div>

	<form action="/albums/store" method="post">
		
		{{ csrf_field() }}

		<div class="form-group">
			<label for="artist">Artist</label>
			<input class="form-control" type="text" id="artist" name="artist">
		</div>

		<div class="form-group">
			<label for="year">Release Year</label>
			<input class="form-control" type="number" id="year" min="1900" max="2030">
		</div>

		<div class="form-group">
			<label for="genre">Genre</label>
			<select class="form-control" name="genre" id="genre">
				<option value="pop">Pop</option>
				<option value="Trending">Trending</option>
				<option value="Chill">Chill</option>
				<option value="Hip Hop">Hip Hop</option>
				<option value="Reggae">Reggae</option>
				<option value="Punk">Punk</option>
				<option value="Electronic">Electronic</option>
			</select>
		</div>

		<button type="submit" class="btn btn-primary">Create album</button>

	</form>

@endsection