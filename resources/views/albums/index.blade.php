@extends('albums.layout')

@section('content')
	
	<h2>All albums</h2>
	
	<div class="row">
		<div class="col-xs-12">
			<a href="/albums/">Show all albums</a><br>
			<a href="/albums/2017">Show albums released in 2017 only</a>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			
			<div class="albums">

				<!-- LOOP OVER THE ALBUM ARTICLE BELOW TO SHOW ALBUMS -->
				<article class="album">

					<img src="http://lorempixel.com/200/200/nightlife/?cache={{ rand() }}" alt="Album title">
					<div class="desc">
						<h2>Artist</h2>
						<h3>Genre</h3>
					</div>

				</article>
				<!-- END OF LOOP -->

			</div>

		</div>
	</div>

@endsection