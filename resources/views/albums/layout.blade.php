<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	 <style>
	 	body
	 	{
	 		font-family: 'Montserrat';

	 	}

	 	.btn-primary, .btn-primary:hover, .btn-primary:active:focus{
	 		background-color: #2AB758 !important;
	 		border-color: #2AB758 !important;
	 	}

	 	.albums{
	 		display: flex;
	 		flex-wrap: wrap;
	 		flex-direction: row;
			background-color: #181818;
			margin: 1em 0;
			padding: 1em;
	 	}

	 	.album{
	 		flex: 18% 0 0;
	 		background-color: #282828;
	 		margin: 1%;
	 	}

	 	.album .desc{
	 		padding: 0 1em;
	 	}

	 	.album h2{
	 		color: white;
	 		font-size: 1em;
	 		margin-bottom: 0;
	 	}

	 	.album h3{
	 		margin-top: 1em;
	 		font-size: 0.9em;
	 		color: #949494;
	 	}

	 </style>
</head>
<body>
	<div class="container-fluid">
		@yield('content')
	</div>
</body>
</html>