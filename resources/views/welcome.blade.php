<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Montserrat', sans-serif;
                
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                
            }

            .title {
                font-size: 64px;
                text-align: center;
            }

            .instructions
            {
                font-family: Helvetica;
            }

            .instructions li{
                margin-bottom: 0.5em;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                <div class="title m-b-md">
                    IMD Laravel Test
                </div>

                <div class="instructions">
                    <ol>
                        <li>Todo: build a simple app that allows us to add music albums to one table [artist name, year, genre]</li>
                        <li>Read the Dusk tests in /tests/Browser/MusicTest.php and make sure they pass</li>
                        <li>Create a migration for the database table. Just one table is enough for this test.</li>
                        <li>[GET] route <strong>/albums/create</strong> should load the correct view (a form is given)</li>
                        <li>[POST] route <strong>/albums/store</strong> is the route that should contain the code that 
                        creates new albums</li>
                        <li>[GET] route <strong>/albums</strong> should display all albums (view given)</li>
                        <li>[GET] route <strong>/albums/{year}</strong> should only display albums from the year {year}</li>
                    </ol>
                </div>
            </div>
        </div>
    </body>
</html>
