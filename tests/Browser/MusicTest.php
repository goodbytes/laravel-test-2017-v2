<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

class MusicTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            // Our Testing Journey begins here
            $faker = Faker::create(); 
            $artist = $faker->name();                               // we'll create a fake music album 
            $year = $faker->numberBetween(2016, 2017);              // with a year
            $genre = $faker->randomElement($array = array ('Chill','Hip Hop','Reggae', 'Punk'));   // and a genre

            // in the test below, we'll visit the /music/create form and enter the fake data we just generated
            $browser->visit('/albums/create')
                    ->assertMissing(".feedback")                    // don't show feedback before saving
                    ->assertTitle('Music App - Create Albums')      // the title of the page should be 'Music App - Create Albums'
                    ->type('artist', $artist)                       // fill up the form here
                    ->type('year', $year)                           // and here
                    ->value('#genre', $genre)                        // and here
                    ->press('Create album')                         // and press the save button
                    
                    // the list should then display this new product on top
                    ->assertVisible('.feedback')
                    ->assertSeeIn(".feedback", "The album was saved");

            // in the test below, we'll visit the /albums page and see if our albums are shown in the list 
            $browser->visit('/albums')
                    ->assertTitle('Music App - All Albums')         // the page title should be 'All Food'
                    ->assertSeeIn(".albums .album:last-child h2", $artist);           // and our newly added food should be shown in the list

            // next, well try out the filter options to see if you can filter albums by year
            $url_good = "/albums/" . $year;  

            $year_bad = ($year == 2017) ? 2016 : 2017;      
            $url_bad = "/albums/$year_bad";
            $browser->visit($url_good)
                    ->assertSeeIn(".albums .album:last-child h2", $artist);           // /albums/2017 should show albums created in 2017 only

            $browser->visit($url_bad)
                    ->assertDontSeeIn(".albums .album:last-child h2", $artist);       // /albums/2016 should not show the new album

        });
    }
}
